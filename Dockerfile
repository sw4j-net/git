FROM debian:stretch-backports

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y -t stretch-backports install git git-lfs gnupg apt-transport-https curl && \
    mkdir -p /root/git && \
    cd /root/git && \
    git init && \
    git lfs install && \
    rm -rf /root/git
